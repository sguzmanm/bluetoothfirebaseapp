const functions = require("firebase-functions");
const admin = require("firebase-admin");
admin.initializeApp();

const db = admin.firestore();

// Listen for changes in all documents in the 'users' collection
exports.useWildcard = functions.firestore
  .document("attendance/{dateId}/students/{studentId}")
  .onCreate((snap, context) => {
    console.log(
      `Attendance for ${context.params.studentId} on ${context.params.dateId}`
    );
    let student = snap.data();
    if (!student.code || !student.name) return 0;

    let studentRef = db
      .collection("studentsList")
      .doc(context.params.studentId);

    return studentRef
      .get()
      .then(doc => {
        if (!doc.exists) {
          console.log("No such document!");
          return 0;
        } else {
          console.log("Document data:", doc.data());
          let qty = doc.data().cantidad;
          if (!qty) qty = 0;
          return studentRef.update({ cantidad: qty + 1 });
        }
      })
      .catch(err => {
        console.log("Error getting document", err);
      });
  });
