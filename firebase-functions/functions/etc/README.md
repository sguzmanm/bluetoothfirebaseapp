# Notes

Inside the app folder you can find the "google-services.json", for you to run the app with my database account.

- Scripts for getting data out of the attendance firebase database is found at scripts/, with the name "copyData.js".
- Inside scripts folder there is a .env file configured with the following keys:
  - firebaseKeyPath: Path to the firebase config json
  - databaseURL: URL for the database at Firebase
  - attendanceBasePath: Base path for the attendance URL web app
- The main firebase function is found at index.js
