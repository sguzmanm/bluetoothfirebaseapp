require("dotenv").config();

const admin = require("firebase-admin");
const serviceAccount = require(process.env.firebaseKeyPath);

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: process.env.databaseURL
});

const db = admin.firestore();

let studentCol = db.collection("studentsList");
let attendanceCol = db.collection("attendance");

exports.addStudentData = data => {
  data.forEach(rawData => {
    let student = rawData.fields;
    console.log("STUDENT", student);
    let studentRef = studentCol.doc(student.code.stringValue);

    let qty = student.cantidad
      ? parseInt(student.cantidad.integerValue, 10)
      : 0;
    let mac = student.mac ? student.mac.stringValue : undefined;
    let name = student.name ? student.name.stringValue : undefined;
    studentRef.set({
      mac: mac,
      code: student.code.stringValue,
      cantidad: qty,
      name: name
    });
  });
};

exports.addAttendanceData = data => {
  for (const key of Object.keys(data)) {
    console.log(key, data[key]);
    let currentRef = attendanceCol.doc(key).collection("students");
    if (!data[key]) continue;
    data[key].forEach(rawData => {
      let student = rawData.fields;
      let studentRef = currentRef.doc(student.code.stringValue);

      let output = { code: student.code.stringValue };
      if (student.timestamp)
        output.timestamp = student.timestamp.timestampValue;
      if (student.mac) output.mac = student.mac.stringValue;
      if (student.name) output.name = student.name.stringValue;

      studentRef.set(output);
    });
  }
};
