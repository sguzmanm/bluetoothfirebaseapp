require("dotenv").config();

const axios = require("axios");
const addData = require("./addData");

let studentData = [];
let attendaceData = [];

async function retrieveUserData(url, token) {
  console.log("DATA", token);
  try {
    const json = await axios.get(url + (token ? "?pageToken=" + token : ""));
    let docs = json.data.documents;

    studentData = studentData.concat(docs);
    console.log("NEXT", json.data);
    console.log(json.data.nextPageToken);
    if (json.data.nextPageToken) {
      await retrieveUserData(url, json.data.nextPageToken);
    }
  } catch (error) {
    console.error(error);
  }
}

const getDateString = date => {
  return date.getDate() + "-" + date.getMonth() + "-" + date.getFullYear();
};

async function retrieveAttendanceData(basePath, date, token) {
  const dateString = getDateString(date);
  try {
    console.log(
      basePath + dateString + "/students" + (token ? "?pageToken=" + token : "")
    );
    const json = await axios.get(
      basePath + dateString + "/students" + (token ? "?pageToken=" + token : "")
    );

    if (!json.data || json.data.length === 0) return;
    let docs = json.data.documents;

    if (docs && token === null) {
      attendaceData[dateString] = [];
    }
    if (docs)
      attendaceData[dateString] = attendaceData[dateString].concat(docs);
    console.log("NEXT", json.data);
    console.log(json.data.nextPageToken);
    if (json.data.nextPageToken) {
      await retrieveAttendanceData(url, json.data.nextPageToken);
    }
  } catch (e) {
    console.error(e);
  }
}

async function fetchAllData() {
  await retrieveUserData(process.env.attendanceBasePath + "studentsList", null);
  console.log("SIZE", studentData.length);
  addData.addStudentData(studentData);

  let tempDate = new Date("03/25/2019");
  let endDate = new Date();

  while (
    tempDate.getDate() !== endDate.getDate() ||
    tempDate.getMonth() !== endDate.getMonth()
  ) {
    await retrieveAttendanceData(
      process.env.attendanceBasePath + "attendance/",
      tempDate,
      null
    );
    console.log(
      getDateString(tempDate),
      attendaceData[getDateString(tempDate)]
    );
    console.log(
      tempDate.getDate(),
      tempDate.getMonth(),
      endDate.getDate(),
      endDate.getMonth()
    );
    tempDate.setDate(tempDate.getDate() + 1);
  }

  console.log("Add data");
  addData.addAttendanceData(attendaceData);
}

fetchAllData();
