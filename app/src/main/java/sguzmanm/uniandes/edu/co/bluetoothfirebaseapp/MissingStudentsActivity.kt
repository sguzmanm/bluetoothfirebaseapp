package sguzmanm.uniandes.edu.co.bluetoothfirebaseapp

import android.Manifest
import android.content.pm.PackageManager
import android.os.Bundle
import android.os.Environment
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.appcompat.app.AppCompatActivity
import android.util.Log
import android.view.View
import com.android.volley.RequestQueue
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.activity_missing_students.*
import sguzmanm.uniandes.edu.co.bluetoothfirebaseapp.CustomExpandableListAdapter.Companion.SEPARATOR
import java.io.File
import java.io.PrintWriter
import java.util.*
import kotlin.collections.ArrayList


import sguzmanm.uniandes.edu.co.bluetoothfirebaseapp.utils.Utils
import sguzmanm.uniandes.edu.co.bluetoothfirebaseapp.utils.Utils.Companion.showTitle

/**
 * Activity for the MissingStudentsActivity
 */
class MissingStudentsActivity: AppCompatActivity() {
    // Log constant
    private val TAG="MissingStudents"
    //Constant for external writing permission
    private val writeExternalStoragePermission=0
    //Map of current students
    private val studentMap=mutableMapOf<String, String>()
    //Result map of absent students
    private var absentStudentHashMap = mutableMapOf<String,List<String>>()
    //Missing element number
    private var missingElements=0


    val db = FirebaseFirestore.getInstance()

    // onCreate
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_missing_students)

        retrieveStudents()

    }

    // Retrieve all students, it receives a queue and nextPageToken if there is another page to look at on firebase
    private fun retrieveStudents(nextPageToken:String = "",queue: RequestQueue?=null){
        val docRef = db.collection("studentsList")

        docRef
            .get()
            .addOnSuccessListener { result ->
                for (document in result) {
                    Log.d(TAG, "${document.id} => ${document.data}")
                    val code=document.data.get("code") as String
                    val name=document.data.get("name") as String
                    studentMap[code]=name
                }
                getCollectionNames().forEach {
                    getMiasingStudentsOfCollection(it)
                }
            }
            .addOnFailureListener { exception ->
                Log.w(TAG, "Error getting documents.", exception)
            }
    }

    // Get all collection names of dates from the first day of class to this day
    private fun getCollectionNames():MutableList<String> {
        val collectionNames= mutableListOf<String>()

        val c = Calendar.getInstance()

        c.set(Calendar.DAY_OF_MONTH, getString(R.string.start_day_of_class).toInt())
        c.set(Calendar.MONTH, getString(R.string.start_month_of_class).toInt())
        c.set(Calendar.YEAR, getString(R.string.start_year_of_class).toInt())

        val currentDate=Calendar.getInstance()
        currentDate.add(Calendar.DATE,1)

        // Add all dates from the first day of class to today
        while(!sameCalendarDate(c,currentDate))
        {
            collectionNames.add("${c.get(Calendar.DAY_OF_MONTH)}-${c.get(Calendar.MONTH)}-${c.get(Calendar.YEAR)}")
            c.add(Calendar.DATE,1)
        }
        missingElements=collectionNames.size

        return collectionNames
    }

    // check if its the same calendar
    private fun sameCalendarDate(c1: Calendar, c2: Calendar):Boolean{
        if(c1.get(Calendar.DAY_OF_MONTH)!=c2.get(Calendar.DAY_OF_MONTH))
            return false

        if(c1.get(Calendar.MONTH)!=c2.get(Calendar.MONTH))
            return false

        if(c1.get(Calendar.YEAR)!=c2.get(Calendar.YEAR))
            return false

        return true
    }

    // Get missing students from a given collection
    private fun getMiasingStudentsOfCollection(collectionName:String) {
        val docRef = db.collection("attendance").document(collectionName).collection("students")

        docRef
            .get()
            .addOnSuccessListener { result ->
                if(result.isEmpty)
                    missingElements--
                else
                {
                    val marked= mutableListOf<String>()

                    for (document in result) {
                        val code=document.data.get("code") as String
                        marked.add(code)
                    }

                    val mapped=studentMap.filter {student-> !marked.contains(student.key) }.toList().map { el->el.first+CustomExpandableListAdapter.SEPARATOR+el.second }
                    absentStudentHashMap[collectionName] = mapped
                    missingElements--

                    // TODO: Check if there are async problems
                    Log.d(TAG,"Current $collectionName for:\n ${absentStudentHashMap[collectionName]?.size} vs ${studentMap.size}")
                }

                if(missingElements<=0)
                {
                    absentStudentHashMap=absentStudentHashMap.toSortedMap(
                        Utils.Companion.FirebaseMapValueComparator()
                    )
                    renderList(absentStudentHashMap)
                }


            }
            .addOnFailureListener { exception ->
                Log.w(TAG, "Error getting documents.", exception)
            }
    }


    // Render list on expandable view
    private fun renderList(map :MutableMap<String,List<String>>) {

        if (map.isEmpty()) {
            showTitle("There are no records of student absences until today", false)
            return
        }

        val expandableListView=expandable_list_view

        if (expandableListView != null) {
            val titleList = ArrayList(map.keys)
            val adapter = CustomExpandableListAdapter(this, titleList.toList(), map)

            expandableListView.setAdapter(adapter)

            expandableListView.setOnGroupExpandListener { groupPosition -> Log.d(TAG, (titleList as ArrayList<String>)[groupPosition] + " List Expanded.") }

            expandableListView.setOnGroupCollapseListener { groupPosition -> Log.d(TAG,(titleList as ArrayList<String>)[groupPosition] + " List Collapsed.") }

            expandableListView.setOnChildClickListener { parent, v, groupPosition, childPosition, id ->
                Log.d(TAG,"Clicked: " + (titleList as ArrayList<String>)[groupPosition] + " -> " + map[(titleList as ArrayList<String>)[groupPosition]]!!.get(childPosition))
                false
            }

            btn_write_missing_students.text=getString(R.string.write_file)
        }
    }

    //
    // Button csv callback
    //
    fun saveCsvFile(v :View)
    {
        if(missingElements>0)
            return

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
        {
            Log.d(TAG,"REQUESTED")
            ActivityCompat.requestPermissions(this,
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                writeExternalStoragePermission)
            return
        }

        writeCsvFile()
    }

    // Write csv file
    private fun writeCsvFile()
    {
        Log.d(TAG,Environment.DIRECTORY_DOWNLOADS)

        btn_write_missing_students.text=getString(R.string.create_file)

        val currentDate=Calendar.getInstance()

        val fileName=getString(R.string.absent_list_prefix)+"_${currentDate.get(Calendar.DAY_OF_MONTH)}_${currentDate.get(Calendar.MONTH)}_${currentDate.get(Calendar.YEAR)}.csv"

        val dest= File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),fileName)
        // response is the data written to file
        PrintWriter(dest).use { out ->

            btn_write_missing_students.text=getString(R.string.loading)

            out.println("Date,Code,Name")
            absentStudentHashMap.forEach {
                it.value.forEach {
                        student->
                    val data=student.split(SEPARATOR)
                    out.println("${it.key},${data[0]},${data[1]}")
                }
            }
            Log.d(TAG,"Write successful")
            showTitle("Your file can be found at ${Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).absolutePath}/$fileName",false)
            btn_write_missing_students.text=getString(R.string.write_file)
        }
    }

    //---------------
    // PERMISSIONS
    //------------------

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        Log.d(TAG,"RECEIVED $requestCode")

        when (requestCode) {
            writeExternalStoragePermission -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    writeCsvFile()
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    showTitle(getString(R.string.file_access_error),true)
                }
                return
            }

            // Add other 'when' lines to check for other
            // permissions this app might request.
            else -> {
                // Ignore all other requests.
            }
        }
    }
}