package sguzmanm.uniandes.edu.co.bluetoothfirebaseapp

import android.app.Activity
import android.app.DatePickerDialog
import android.bluetooth.BluetoothAdapter
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.BitmapDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.firebase.FirebaseApp
import com.google.firebase.firestore.FieldValue
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.synthetic.main.activity_main.*
import sguzmanm.uniandes.edu.co.bluetoothfirebaseapp.utils.Utils.Companion.showTitle
import java.io.ByteArrayOutputStream

import java.util.*


class MainActivity : AppCompatActivity() {

    val TAG="Bluetooth"
    private val bluetoothDiscoverable=0

    private var data:String?=null
    private val bluetoothDuration=120

    val db = FirebaseFirestore.getInstance()
    val storage = FirebaseStorage.getInstance()
    // Points to "images"
    val imagesRef = storage.reference.child("images")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        updateCurrentProfile()
    }

    fun bluetoothCallback (v: View){
        if(data!=null)
            return

        Log.d(TAG,"Bluetooth start call")
        startActivityForResult(Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE),bluetoothDiscoverable)
    }

    fun firebaseCallback(v: View){
        checkListName(getString(R.string.student_code))
    }

    fun goToMissingStudents(v:View){
        val intent = Intent(this, MissingStudentsActivity::class.java)
        // start your next activity
        startActivity(intent)
    }

    fun goToStoreData(v: View){
        val intent = Intent(this, StorageActivity::class.java)
        // start your next activity
        startActivity(intent)
    }

    fun clickDataPicker(view: View) {
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
            // Display Selected date in Toast
            Toast.makeText(this, """$dayOfMonth - ${monthOfYear + 1} - $year""", Toast.LENGTH_LONG).show()
            addAssistance(dayOfMonth,monthOfYear,year)
        }, year, month, day)
        dpd.show()
    }

    private fun addAssistance(day:Int, month:Int, year:Int)
    {
        val data = hashMapOf(
            "code" to getString(R.string.student_code),
            "mac" to getString(R.string.student_mac),
            "name" to getString(R.string.student_name),
            "timestamp" to FieldValue.serverTimestamp()        )

        db.collection("attendance").document("$day-$month-$year").collection("students").document(getString(R.string.student_code))
            .set(data)
            .addOnSuccessListener {
                Log.d(TAG,"Success adding fir $day-$month-$year")
                updateCurrentProfile()  }
            .addOnFailureListener { e ->
                showTitle("Error adding info to activity",true)
                Log.w(TAG, "Error adding document", e)
            }
    }

    private fun updateCurrentProfile()
    {
        val docRef = db.collection("studentsList").document(getString(R.string.student_code))

        docRef.addSnapshotListener { snapshot, e ->
            if (e != null) {
                Log.w(TAG, "Listen failed.", e)
                return@addSnapshotListener
            }

            if (snapshot != null && snapshot.exists()) {
                Log.d(TAG, "My data: ${snapshot.data}")

                val student=snapshot.data
                if(student==null || !student.containsKey("code"))
                    return@addSnapshotListener

                val msg="Code: ${student.get("code")}\n The student ${student.get("name")} number of classes: ${if(!student.containsKey("cantidad")) 0 else student.get("cantidad")}"
                personalInfo.text=msg
            } else {
                Log.d(TAG, "Current data: null")
            }
        }
    }

    private fun checkListName(studentCode:String)
    {
        val calendar=Calendar.getInstance()
        val day=calendar.get(Calendar.DAY_OF_MONTH)
        val month=calendar.get(Calendar.MONTH)
        val year=calendar.get(Calendar.YEAR)

        Log.d(TAG,"Getting $studentCode attendance at $day-$month-$year")


        val docRef = db.collection("attendance").document("$day-$month-$year").collection("students").document("$studentCode")

        docRef.addSnapshotListener { snapshot, e ->
            if (e != null) {
                Log.w(TAG, "Listen failed.", e)
                return@addSnapshotListener
            }

            if (snapshot != null && snapshot.exists()) {
                Log.d(TAG, "Current data: ${snapshot.data}")
                if(snapshot.data?.containsKey("code")!!) {
                    btn_bluetooth.text=getString(R.string.bluetooth_active)
                    btn_bluetooth.setBackgroundColor(Color.GREEN)
                    btn_firebase.text=getString(R.string.check_firebase)
                }
            } else {
                Log.d(TAG, "Current data: null")
            }
        }

        btn_firebase.text=getString(R.string.check_real_time)
    }

    private fun isBluetoothDiscoveryFinished (currentMode:Int):Boolean
    {
        return (currentMode== BluetoothAdapter.SCAN_MODE_CONNECTABLE || currentMode == BluetoothAdapter.SCAN_MODE_NONE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        if (requestCode==bluetoothDiscoverable)
        {
            if(resultCode== bluetoothDuration)
            {
                Log.d(TAG,"Activity discovery on")
                // Register for broadcasts when a device is discovered.
                val filter = IntentFilter(BluetoothAdapter.ACTION_SCAN_MODE_CHANGED)
                btn_bluetooth.text="Starting discovery"
                registerReceiver(receiver, filter)
                return
            }

            if (resultCode== Activity.RESULT_CANCELED)
            {
                // TODO: Handle error
                Log.e(TAG,"Bluetooth discovery was disabled")
                showTitle(getString(R.string.bluetooth_discovery_error),true)
                return
            }

            // TODO: Handle error
            Log.e(TAG,"Bluetooth unknown error $resultCode")
            showTitle(getString(R.string.unknown_error),true)
            return
        }
    }

    // Create a BroadcastReceiver.
    private val receiver = object : BroadcastReceiver() {

        override fun onReceive(context: Context, intent: Intent) {
            val action: String = intent.action
            Log.d(TAG,"Action $action")
            when(action) {
                BluetoothAdapter.ACTION_SCAN_MODE_CHANGED -> {
                    // Discovery has found a device. Get the BluetoothDevice
                    // object and its info from the Intent.
                    val currentMode: Int =
                        intent.getIntExtra(BluetoothAdapter.EXTRA_SCAN_MODE,BluetoothAdapter.SCAN_MODE_NONE)

                    if(isBluetoothDiscoveryFinished(currentMode) && data==null)
                    {
                        checkListName(getString(R.string.student_code))
                    }
                }
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        // Don't forget to unregister the ACTION_FOUND receiver.
        try {
            unregisterReceiver(receiver)
        }
        catch(e:Exception)
        {
            // TODO: Handle error
            Log.e(TAG,e.message)
            showTitle("Error: ${e.message}",true)

        }
    }


}
