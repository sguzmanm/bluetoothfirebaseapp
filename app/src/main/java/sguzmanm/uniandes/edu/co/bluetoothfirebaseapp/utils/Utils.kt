package sguzmanm.uniandes.edu.co.bluetoothfirebaseapp.utils

import android.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.firestore.FirebaseFirestore
import sguzmanm.uniandes.edu.co.bluetoothfirebaseapp.R
import java.util.Comparator

class Utils {
    //---------------
    // Custom functions
    //---------------
    companion object{

        // Show alert dialog with title
        fun AppCompatActivity.showTitle(msg:String, isError: Boolean) {
            // Initialize a new instance of
            val builder = AlertDialog.Builder(this)

            // Set the alert dialog title
            val title=if(isError){getString(R.string.error_title)} else{getString(R.string.alert_title)}
            builder.setTitle(title)

            // Display a message on alert dialog
            builder.setMessage(msg)


            // Display a neutral button on alert dialog
            builder.setNeutralButton(getString(R.string.accept_dialog)){ _, _ ->
            }

            // Finally, make the alert dialog using builder
            val dialog: AlertDialog = builder.create()

            // Display the alert dialog on app interface
            dialog.show()
        }

        // Comparator for string date order
        class FirebaseMapValueComparator() : Comparator<String> {

            override fun compare(o1: String, o2: String): Int
            {
                val c1=o1.split("-").map { it.toInt() }
                val c2=o2.split("-").map{it.toInt()}

                if(c1[2]>c2[2])
                    return 1

                if(c1[2]<c2[2])
                    return -1

                if(c1[1]>c2[1])
                    return 1

                if(c1[1]<c2[1])
                    return -1

                if(c1[0]>c2[0])
                    return 1

                if(c1[0]<c2[0])
                    return -1

                return 0
            }
        }

    }
}