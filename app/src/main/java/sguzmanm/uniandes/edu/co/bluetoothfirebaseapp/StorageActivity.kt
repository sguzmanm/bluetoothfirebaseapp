package sguzmanm.uniandes.edu.co.bluetoothfirebaseapp


import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.View
import com.google.firebase.FirebaseApp
import com.google.firebase.storage.FirebaseStorage
import kotlinx.android.synthetic.main.activity_store_photo.*
import sguzmanm.uniandes.edu.co.bluetoothfirebaseapp.utils.Utils.Companion.showTitle
import java.io.ByteArrayOutputStream

import java.util.*


class StorageActivity : AppCompatActivity() {

    val TAG="Storage"
    val REQUEST_IMAGE_CAPTURE=1
    var taken=false

    val storage = FirebaseStorage.getInstance()
    // Points to "images"
    val imagesRef = storage.reference.child("images")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_store_photo)

        FirebaseApp.initializeApp(this)
    }

    fun takePic(v :View){
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(packageManager)?.also {
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
            }
        }
    }


    fun firebaseStorageCallback(v: View){
        if(!taken)
            return
        // Points to "images/space.jpg"
        // Note that you can use variables to create child values
        val fileName = "${Date().time}-img.jpg"
        val spaceRef = imagesRef.child(fileName)

        // Get the data from an ImageView as bytes
        val bitmap = (imagePreview.drawable as BitmapDrawable).bitmap
        val baos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
        val data = baos.toByteArray()

        var uploadTask = spaceRef.putBytes(data)
        uploadTask.addOnFailureListener {
            // Handle unsuccessful uploads
            Log.d(TAG,"FAiled"+it.message)
            showTitle(getString(R.string.firebase_storage_error),true)
        }.addOnSuccessListener {
            showTitle("Success saving file for ${spaceRef.name}",false)
            Log.d(TAG,"Success "+it.metadata.toString())
        }

    }

    // Results of intents
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {
            if(data==null)
                return
            val imageBitmap = data.extras.get("data") as Bitmap
            imagePreview.setImageBitmap(imageBitmap)
            taken=true
            btn_store.text=getString(R.string.store_data)
        }
    }



}
