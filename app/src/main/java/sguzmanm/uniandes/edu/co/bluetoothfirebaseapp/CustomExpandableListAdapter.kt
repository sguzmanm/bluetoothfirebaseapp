package sguzmanm.uniandes.edu.co.bluetoothfirebaseapp

import android.content.Context
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.TextView

import java.util.HashMap

class CustomExpandableListAdapter (private val context: Context, private val titleList: List<String>, private val dataList: MutableMap<String, List<String>>) : BaseExpandableListAdapter() {

    /**
     * Companion objects
     */
    companion object{

        const val SEPARATOR = ";;;"

    }

    override fun getChild(listPosition: Int, expandedListPosition: Int): Any {
        val dataList = this.dataList[this.titleList[listPosition]]
        if (dataList != null)
        {
            val listVal=dataList[expandedListPosition]
            return listVal.replace(SEPARATOR," ")
        }

        return Any()
    }

    override fun getChildId(listPosition: Int, expandedListPosition: Int): Long {
        return expandedListPosition.toLong()
    }

    override fun getChildView(listPosition: Int, expandedListPosition: Int, isLastChild: Boolean, startConvertView: View?, parent: ViewGroup): View {
        var convertView = startConvertView
        val expandedListText = getChild(listPosition, expandedListPosition) as String

        if (convertView == null) {
            val layoutInflater = this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = layoutInflater.inflate(R.layout.list_item, null)
        }

        if(convertView!=null)
        {
            val expandedListTextView = convertView.findViewById<TextView>(R.id.expanded_list_item)
            expandedListTextView.text = expandedListText
        }

        return convertView?:View(this.context)
    }

    override fun getChildrenCount(listPosition: Int): Int {
        return this.dataList[this.titleList[listPosition]]!!.size
    }

    override fun getGroup(listPosition: Int): Any {
        return this.titleList[listPosition]
    }

    override fun getGroupCount(): Int {
        return this.titleList.size
    }

    override fun getGroupId(listPosition: Int): Long {
        return listPosition.toLong()
    }

    override fun getGroupView(listPosition: Int, isExpanded: Boolean, startConvertView: View?, parent: ViewGroup): View{
        var convertView = startConvertView
        val listTitle = getGroup(listPosition) as String

        if (convertView == null) {
            val layoutInflater = this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = layoutInflater.inflate(R.layout.list_group, null)
        }

        if(convertView!=null)
        {
            val listTitleTextView = convertView.findViewById<TextView>(R.id.list_title)
            listTitleTextView.setTypeface(null, Typeface.BOLD)
            listTitleTextView.text = listTitle
        }

        return convertView?:View(this.context)
    }

    override fun hasStableIds(): Boolean {
        return false
    }

    override fun isChildSelectable(listPosition: Int, expandedListPosition: Int): Boolean {
        return true
    }
}
